# Future Outlook

1. [TensorRT](#tensorrt)
1. [Improved UI](#improved-ui)
1. [Extend Use Case](#extend-use-case)

## TensorRT

While the YOLOv8 inference serves our purpose well, optimizing it further with [TensorRT](https://developer.nvidia.com/tensorrt) could provide notable benefits. TensorRT, designed by NVIDIA, is renowned for accelerating deep learning models efficiently (usually yielding a 5x uplift in inference speed). Its ability to convert trained models into highly optimized runtime engines means that deployment can be executed faster while maintaining model accuracy. Regrettably, during the course of our project, we faced challenges integrating TensorRT for our inference needs. Yet, there's no doubt that mastering its integration could unlock significant performance enhancements for our model in the future which would unlock further use cases such as streaming the video to the frontend via WebRTC. Nvidia also provides a highly abstracted server offering an `http` and `grpc` endpoint in its [Trition Inference Server](https://developer.nvidia.com/triton-inference-server). Any of the TensorRT specific solutions would also profit from the framework agnostic solution with a plug and play like environment for different trained models.

## Improved UI

Our current user interface has served its purpose commendably as a prototype. It provides users with clear visuals and functionality, ensuring that they can interact with our system effectively. However, like any prototype, there's always room for refinement. One crucial area of enhancement is error handling, ensuring that users are not just alerted to issues but are also guided on how to resolve them. Furthermore, considering the feedback and the evolving needs of our users, streaming the video feed from the Jetson Nano could be a valuable addition. By suppressing the bounding boxes and emphasizing only the relevant parts or states, users could focus on critical information, improving both the user experience and the efficiency of the assembly or disassembly process.

## Improve Edge Device

The Jetson Nano, while an impressive edge device in its own right, has its limitations, especially when juxtaposed against the rapid advancements in IoT technology. The computational and memory constraints can sometimes be limiting factors, especially for computationally intensive tasks such as real-time image processing and deep learning inference. Moving forward, to ensure that our system remains at the cutting edge of performance and compatibility, it would be prudent to consider upgrading to a more modern IoT device. Newer devices not only offer enhanced computational capabilities but also better compatibility with the latest versions of software libraries and frameworks. By doing so, we can ensure smoother integration, better performance, and a more future-proof solution, allowing us to continuously innovate and improve upon our current capabilities.

## Extend Use Case

The potential applications of our system are not limited to the current scope. An immediate and obvious extension would be the inclusion of a wider variety of Lego parts and even entire Lego robot designs. By expanding the database and training the model on a larger spectrum of components and configurations, we can cater to a broader audience, from hobbyists to professional Lego builders. This would not only enhance the versatility of our tool but also provide users with a richer experience, offering guidance on more intricate and complex designs. Moreover, diversifying the range of supported Lego constructs could pave the way for educational programs or workshops, where learners can benefit from real-time feedback and assistance as they experiment and create.

## Generate extra Test Data

While our current approach to dataset generation, which primarily uses video-extracted frames, has served its purpose, there are inherent challenges associated with potential data leakage and the accurate assessment of model generalization. Recognizing the limitations of random assignment from continuous video footage and the consequential temporal correlations, we would implement substantial improvements in the future versions of our dataset.

**Proposed Enhancements:**

1. **Distinct Test Set**: We would prioritize the creation of an entirely distinct test set. This set would consist of images completely uncorrelated from the training and validation sets, ensuring a genuine measure of model performance on truly unseen data.
2. **Improved Data Splitting**: Instead of random frame distribution, we would use multiple videos for the dataset and categorize them, ensuring a split like 70/20/10 at the video level. Post categorization, frames would extracted and solely used within their designated sets. This method would effectively eliminate data leakage between sets.
3. **Varied Frame Extraction**: Given the constraints of video-derived datasets, we would use a lower frame-per-second (fps) rate for the validation and test sets. This would not only conserve resources but also ensure that the datasets remain relatively balanced.
4. **Diverse Data Capture**: To enhance the richness of our dataset, we'll expand our data collection to involve varied lighting conditions, multiple camera types, diverse backgrounds, and different vantage points.

By embracing these changes, we would aim to strike a better balance between the need for diverse training data and the essential requirement of a truly independent and representative test set. These measures would solidify a dataset that serves as a reliable foundation for deep learning models, ensuring they perform optimally in real-world scenarios.
