# Model Choice

1. [YOLO (You Only Look Once)](#yolo-you-only-look-once)
2. [RCNNs (Region-based Convolutional Neural Networks)](#rcnns-region-based-convolutional-neural-networks)
3. [Comparison and Considerations for NVIDIA's Jetson Nano](#comparison-and-considerations-for-nvidias-jetson-nano)

## **YOLO (You Only Look Once)**

You Only Look Once, commonly known as YOLO, revolutionized the domain of real-time object detection. At its core, YOLO differentiates itself by identifying and classifying objects in images through a singular forward pass. Instead of evaluating myriad regions within an image as many other models do, YOLO assesses the entire image in one go. This process involves dividing the image into an $SxS$ grid and predicting multiple bounding boxes along with class probabilities for each grid cell. Each bounding box comprises predictions related to its coordinates, dimensions, and a confidence score, ensuring the system recognizes the object's position and size accurately. The loss function in YOLO plays a critical role, as it comprises components for classification loss, localization loss, and confidence loss. One of YOLO's primary benefits is its rapid processing speed, making it a prime candidate for real-time applications. Moreover, its ability to view the entire image during both training and testing allows it to capture contextual nuances about class appearances more effectively than some competing models.

1. Redmon, J., Divvala, S., Girshick, R., & Farhadi, A. (2016). You only look once: Unified, real-time object detection. In Proceedings of the IEEE conference on computer vision and pattern recognition (pp. 779-788).
2. Girshick, R. (2015). Fast R-CNN. In Proceedings of the IEEE international conference on computer vision (pp. 1440-1448)
3. Redmon, J., & Farhadi, A. (2018). YOLOv3: An Incremental Improvement. arXiv preprint arXiv:1804.02767.
4. Howard, A. G., Zhu, M., Chen, B., Kalenichenko, D., Wang, W., Weyand, T., ... & Adam, H. (2017). Mobilenets: Efficient convolutional neural networks for mobile vision applications. arXiv preprint arXiv:1704.04861.
5. He, K., Zhang, X., Ren, S., & Sun, J. (2016). Deep residual learning for image recognition. In Proceedings of the IEEE conference on computer vision and pattern recognition (pp. 770-778).

## **RCNNs (Region-based Convolutional Neural Networks)**

Region-based Convolutional Neural Networks (RCNNs) take a different approach to object detection. Instead of analyzing an image in its entirety in a single pass, RCNNs first propose potential bounding boxes or regions within the image using algorithms like Selective Search, generating around 2000 such region proposals. Once these regions are identified, each one is resized to a uniform dimension and passed through a pretrained convolutional neural network (CNN) to distill features. These extracted features are then processed by Support Vector Machines (SVMs) for object classification within the proposals. Concurrently, a linear regressor refines the bounding box coordinates. Over time, this approach has been refined: Fast R-CNN optimized the original by extracting features from the entire image just once, and Faster R-CNN further accelerated the process with the introduction of the Region Proposal Network (RPN) that shared features with the detection network.

1. Girshick, R., Donahue, J., Darrell, T., & Malik, J. (2014). Rich feature hierarchies for accurate object detection and semantic segmentation. In Proceedings of the IEEE conference on computer vision and pattern recognition (pp. 580-587).
2. Uijlings, J. R., Van De Sande, K. E., Gevers, T., & Smeulders, A. W. (2013). Selective search for object recognition. International journal of computer vision, 104(2), 154-171.
3. Girshick, R. (2015). Fast R-CNN. In Proceedings of the IEEE international conference on computer vision (pp. 1440-1448).
4. Ren, S., He, K., Girshick, R., & Sun, J. (2015). Faster R-CNN: Towards real-time object detection with region proposal networks. In Advances in neural information processing systems (pp. 91-99).
5. He, K., Zhang, X., Ren, S., & Sun, J. (2016). Deep residual learning for image recognition. In Proceedings of the IEEE conference on computer vision and pattern recognition (pp. 770-778).

## **Comparison and Considerations for NVIDIA's Jetson Nano**

When comparing YOLO and RCNNs, several distinctions emerge. YOLO stands out for its unparalleled speed, primarily because it processes an image in a single forward pass, whereas RCNNs demand multiple passes for each region proposal. Though RCNNs initially had an edge in accuracy due to their detailed region proposals, advancements in YOLO have steadily bridged this gap. From an implementation standpoint, YOLO offers a more streamlined architecture, devoid of the complexities associated with region proposal mechanisms inherent to RCNNs. However, Faster R-CNN did reduce this complexity to some extent.

Choosing between YOLO and RCNNs often boils down to specific task requirements. If an application mandates real-time detection, YOLO's speed makes it a frontrunner. Furthermore, its ability to capture broader contextual details sometimes gives it an edge in intricate scenes. However, for applications where extreme accuracy is pivotal, and computational time isn't a primary concern, RCNN variants may be more fitting.

For the edge device _NVIDIA Jetson Nano_, resource constraints play a significant role in model choice. The Jetson Nano, while powerful for its size, has limitations in memory and compute capacity compared to full-scale GPUs. YOLO, especially its smaller versions, may be more suitable for such a device due to its reduced computational demands. In contrast, deploying an RCNN on the Jetson Nano might demand more resources, potentially leading to slower inference times and reduced overall performance. Given these constraints and real-time applications on the Jetson Nano, we chose YOLO as it would likely be the more efficient and practical for our use case.
