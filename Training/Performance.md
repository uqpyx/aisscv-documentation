# Performance

1. [Yolov5](#yolov5)
1. [Yolov7](#yolov7)
1. [Yolov8](#yolov8)

In the context of deep learning and object detection, the constant evolution of algorithms ensures that there are always newer, and often more efficient, versions to explore. The YOLO (You Only Look Once) architecture, in particular, has seen rapid advancements over a short span, each version bringing forth improvements over its predecessor. As we embarked on our project, the need for robust and efficient object detection was paramount. But with multiple versions of YOLO available, which one would serve our needs best?

In this section, we delve deep into a comprehensive performance evaluation of three YOLO versions: YOLOv5, YOLOv7, and YOLOv8. But the choice of architecture is only one piece of the puzzle. The quality and nature of the training data can significantly influence the model's efficacy. To this end, we also compare the outcomes yielded by different datasets in our possession.

Our performance metrics, captured after extensive testing, are presented in the tables below. They provide insights into the accuracy, speed, and resource utilization of each combination, helping us determine the most optimal configuration for our specific use-case.

As you navigate through the results, we encourage readers to not just focus on the numbers but to understand the subtle trade-offs between accuracy and efficiency, and how the choice of data can sometimes overshadow even the most sophisticated algorithms.

The model that was used in the final deployment on the jetson nano is marked in bold as it seemed to be the best tradeoff between inference speed, precision, recall and map-precision scores. These results are only an excerpt. The full list can be viewed on GitLab and is linked in the sections for each respective framework.

Let the numbers guide your understanding, and may the best configuration win!

---

## YOLOv5

| Dataset     | Sub-Model | Weights    | Epochs | Precision | Recall | MAP50 | MAP50-95 |
| ----------- | --------- | ---------- | ------ | --------- | ------ | ----- | -------- |
| v1_0        | m         | yolov5m.pt | 300    | 0.82      | 0.856  | 0.867 | 0.644    |
| v1_0        | n         | yolov5n.pt | 300    | 0.81      | 0.861  | 0.859 | 0.599    |
| v1_0        | s         | yolov5s.pt | 300    | 0.819     | 0.857  | 0.866 | 0.627    |
| v1_0        | s         | No weights | 300    | 0.821     | 0.859  | 0.865 | 0.63     |
| v1_1        | s         | yolov5s.pt | 300    | 0.846     | 0.844  | 0.859 | 0.624    |
| v3_0/vfinal | n         | No weights | 300    | 0.921     | 0.923  | 0.952 | 0.777    |
| v3_0/vfinal | n         | yolov5n.pt | 300    | 0.932     | 0.937  | 0.957 | 0.811    |
| v3_0/vfinal | s         | No weights | 300    | 0.932     | 0.928  | 0.957 | 0.805    |
| v3_0/vfinal | s         | yolov5s.pt | 300    | 0.94      | 0.94   | 0.962 | 0.832    |
| v3_0/vfinal | m         | No weights | 300    | 0.939     | 0.934  | 0.956 | 0.824    |
| v3_0/vfinal | m         | yolov5m.pt | 300    | 0.934     | 0.936  | 0.957 | 0.838    |

See the full list of trained models in the [yolov5 model repository](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov5.git).

## YOLOv7

| Dataset     | Sub-Model | Weights   | Epochs | Precision | Recall | MAP50 | MAP50-95 |
| ----------- | --------- | --------- | ------ | --------- | ------ | ----- | -------- |
| v1_1        | tiny      | yolov7.pt | 300    | 0.789     | 0.877  | 0.856 | 0.602    |
| v3_0/vfinal | tiny      | yolov7.pt | 300    | 0.933     | 0.926  | 0.954 | 0.802    |

## YOLOv8

| Dataset         | Sub-Model | Weights        | Epochs  | Precision | Recall    | MAP50     | MAP50-95  |
| --------------- | --------- | -------------- | ------- | --------- | --------- | --------- | --------- |
| v1_0            | s         | yolov8s.pt     | 300     | 0.81      | 0.85      | 0.875     | 0.677     |
| v1_1            | n         | yolov8n.pt     | 300     | 0.802     | 0.853     | 0.861     | 0.656     |
| **v3_0/vfinal** | **n**     | **yolov8n.pt** | **296** | **0.926** | **0.946** | **0.966** | **0.854** |
| v3_0/vfinal     | n         | none           | 300     | 0.923     | 0.932     | 0.959     | 0.832     |
| v3_0/vfinal     | n         | yolov8n.pt     | 300     | 0.924     | 0.93      | 0.96      | 0.828     |
| v3_0/vfinal     | s         | none           | 300     | 0.926     | 0.946     | 0.961     | 0.839     |
| v3_0/vfinal     | s         | yolov8s.pt     | 300     | 0.922     | 0.941     | 0.959     | 0.837     |
| v3_0/vfinal     | m         | none           | 300     | 0.941     | 0.932     | 0.964     | 0.852     |
| v3_0/vfinal     | m         | yolov8m.pt     | 300     | 0.919     | 0.947     | 0.962     | 0.848     |

See the full list of trained models in the [yolov8 model repository](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov8.git).
