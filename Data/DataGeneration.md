# Data Generation

1. [Data Generation](#data-generation)
2. [Recording](#recording)
   - [Introduction](#introduction)
   - [Data Recording Strategy](#data-recording-strategy)
   - [Video-based Image Collection](#video-based-image-collection)
   - [Diversity in Capture Conditions](#diversity-in-capture-conditions)
     - [Lighting Conditions](#lighting-conditions)
     - [Vantage Points](#vantage-points)
     - [Background Variability](#background-variability)
   - [Equipment Variability](#equipment-variability)
     - [iPhone Camera](#iphone-camera)
     - [Android Camera](#android-camera)
   - [Conclusion](#conclusion)
3. [Labeling](#labeling)

   - [Labeling Workflow](#labeling-workflow)
     - [Video to Frame Conversion](#video-to-frame-conversion)
     - [Assigning Labeling Tasks](#assigning-labeling-tasks)
     - [Dataset Construction](#dataset-construction)
       - [Training Set](#training-set)
       - [Validation Set](#validation-set)
       - [Testing Set](#testing-set)
   - [Question of Random Assignment for Video-extracted Frames: Balancing Training Diversity and Test Set Integrity](#question-of-random-assignment-for-video-extracted-frames-balancing-training-diversity-and-test-set-integrity)
   - [Conclusion](#conclusion)

4. [Pre-processing](#pre-processing)
   - [Introduction](#introduction)
   - [Auto-Orientation](#auto-orientation)
     - [Roboflow's Implementation](#roboflows-implementation)
   - [Resizing to 640x640 Pixels](#resizing-to-640x640-pixels)
     - [Roboflow's Implementation](#roboflows-implementation)
5. [Augmentation](#augmentation)
   - [Introduction](#introduction)
   - [Roboflow Augmentation in V1_0](#roboflow-augmentation-in-v10)
     - [Implementation in V1_0](#implementation-in-v10)
   - [Comparative Analysis: V1_0 vs. V1_1](#comparative-analysis-v10-vs-v11)
   - [YOLO's Inherent Augmentation Capabilities](#yolos-inherent-augmentation-capabilities)
     - [YOLO's Augmentation](#yolos-augmentation)
   - [Final Version (v3_0/vfinal) Decision](#final-version-vfinalv30-decision)

## Recording

The quality and diversity of training data play a pivotal role in determining the performance of deep learning models, especially in computer vision tasks. Recognizing the importance of capturing a wide range of variabilities, we devised a comprehensive data recording strategy to create a dataset for our application.

### **Data Recording Strategy**

### Video-based Image Collection:

Rather than capturing individual still images, we adopted a video-based data collection approach. Recording videos of the different parts and their respective states allowed for the seamless generation of a plethora of frames, which could later be converted to individual images. This methodology ensured a high volume of images, capturing subtle nuances and transitions between states. To avoid too high correlation between different images, we opted for only approximately one frame per second that is extracted from videos.

### Diversity in Capture Conditions:

1. **Lighting Conditions**: Understanding that real-world lighting conditions can vary greatly, we recorded videos under different lighting scenarios. This ensured that our dataset captured the parts and states under various illuminations, shadows, and brightness levels, making our model robust against such variabilities.
2. **Vantage Points**: Recognizing that objects can be viewed from numerous angles in practical scenarios, we varied our recording angles and heights. This diversity ensured the model would be exposed to potential real-world viewing perspectives during training, promoting better generalization. Several videos with and without hands in them, partially covering parts and states were included.
3. **Background Variability:** Alongside the considerations for lighting and vantage points, we also ensured that videos were shot against varied backgrounds. This step was crucial as backgrounds can introduce additional complexities or distractions in an image. By training the model with a variety of backdrops, we aimed to enhance its ability to focus on the target objects, irrespective of the surrounding environment.

### Equipment Variability:

To further enhance the dataset's richness, we employed two types of cameras:

1. **iPhone Camera**: Leveraging the high-resolution camera capabilities of modern iPhones, we captured videos that provide fine details and vibrant color profiles.
2. **Android Camera**: In parallel, using an Android camera ensured that our dataset also included the variations and characteristics that come from other smartphones.

This dual-camera strategy aimed to introduce subtle differences in color reproduction, focal lengths, and depth perceptions, further augmenting the dataset's variability.

### **Conclusion**

Our multifaceted data recording strategy was aimed at curating a dataset that not only was voluminous but also was representative of a wide range of conditions and variabilities that the model might encounter in real-world applications. By leveraging video recordings, diverse capture conditions, and multiple recording devices, we endeavored to build a robust dataset that would serve as a solid foundation for our deep learning tasks.

## **Labeling**

Data labeling is a critical and long phase in computer vision projects. Precise annotation ensures that the models are exposed to accurate ground truths, which subsequently determine their predictive performance. We present a detailed methodology of our data labeling process, undertaken to increase the reliability and diversity of our dataset.

### **Labeling Workflow**

### 1. Video to Frame Conversion:

Upon recording, our video files were uploaded to Roboflow, a platform known for its capabilities in processing and managing image datasets for machine learning. Here, we initiated the frame extraction process.

To prioritize diversity and reduce temporal correlation between subsequent frames, we set the extraction rate at approximately 1 frame per second. This ensured that the images were distinct and captured varied states and conditions, as opposed to consecutively similar frames which can induce redundancy.

### 2. Assigning Labeling Tasks:

The extracted frames were then divided into labeling tasks. Each task was allocated to a different member of our team. By distributing the work, we ensured the labeling process was efficient while also integrating multiple perspectives, leading to a potential reduction in individual bias or oversight.

### 3. Dataset Construction:

Once all members completed their respective labeling tasks, the annotated images were aggregated. These images were then partitioned into distinct subsets: training, validation, and testing. The split ratios used were 70%, 20%, and 10% respectively. The partitioning was executed through random assignment to ensure an unbiased distribution of data.

**Significance of Data Splits**:

- **Training Set (70%)**: This subset is the primary data used to train the model. The algorithms iteratively adjust the model's parameters based on the input-output associations in this set, enabling the model to learn and generalize.
- **Validation Set (20%)**: The validation set acts as an intermediate checkpoint. During training, after certain epochs or iterations, the model's performance is evaluated on this set. It helps in hyperparameter tuning, model selection, and to diagnose issues like overfitting. Essentially, it's a tool to ensure the model is learning effectively without memorizing the training data.
- **Testing Set (10%)**: This subset is held back until the very end. Once the model is trained and optimized using the training and validation sets, the testing set provides an unbiased evaluation metric, gauging the model's performance on unseen data. It's indicative of how the model might perform in real-world scenarios. This data is not to be touched until the very end as it would otherwise lead to data leakage.

### Question **of Random Assignment for Video-extracted Frames: Balancing Training Diversity and Test Set Integrity**

Random assignment of images to different data subsets is a prevalent data splitting technique. Continuous video footage inherently possesses temporal correlations. This implies that frames extracted from a video might only be minor variations or contain near-identical characteristics.

When frames with temporal proximity land in both the training and test sets, it can leads to testing inaccuracies. Because the model has trained on images that are nearly identical, the test set fails to offer a truly distinct set of images. Consequently, this can distort the model's performance metrics due to potential data leakage. Moreover, the metric does not consider that the model may simply be overfitting to the specific use case, rather than improving and achieving generalization. Additionally, metrics then do not offer proper inter model comparability.

Thus, if pictures are extracted as frames from videos, they should not be randomly distributed among the train, validation and test set, as this would produce exactly the aforementioned effects. Ideally, all images are completely different and thus not even taken from a video setting with extracted frames. Generating such a dataset is however very time consuming and thus less realistic to the scale we wanted to achieve. One then has to balance the correlation between frames resulting from a video and the labor necessary to generate totally uncorrelated pictures. Using frames is thus an acceptable alternative, so long as the distribution to the sets has been considered. This presents an aspect in this project that needs to be looked at more into in the future outlook.

In our dataset, we chose to only extract a frame per second and record videos with various lighting conditions, vantage points and backgrounds. Furthermore, different cameras were chosen to generate the dataset. All these measures were taken in order to create a more representative dataset. However, the ideal approach to splitting into train, validation and test set would avoid randomization as it can introduce the effects described above. Instead, one could use multiple videos and make a 70/20/10 split of the videos (not its frames). After assignment to the designated set, frames could be extracted and exclusively used in the designated set. This way, data leakage would be prevented. Arguably, an even lower fps for extraction of frames can be used for the validate and test set as the size thereof is limited to keep relative balance. With that, generalization can be evaluated more appropriately.

So, the crux of the matter lies in balancing these priorities:

1. Ensure the training set is varied, capturing the richness and diversity of the data domain.
2. Keep the test set uncorrelated from the training data to get an authentic measure of the model's generalization (usually strong temporal correlation is introduced through videos; this is critical as soon as these correlated frames are shared across train, validation and test set).

### **Conclusion**

Our data labeling methodology for video-extracted frames incorporated random assignment to achieve diversity in the dataset. However, upon reflection, the ideal approach might have involved creating a distinct set of test images, ensuring they're entirely uncorrelated to the training set. This would provide an even more reliable gauge of our deep learning model's performance on truly unseen data. Despite the challenges, we remained committed to establishing a rich, accurate, and representative foundation for our model to learn from.

## Pre-processing

The efficacy of image-based machine learning models is often influenced not only by the model architecture but also by the quality and format of the input data. Ensuring images are uniformly processed can aid in achieving consistent and improved model performance. In this regard, we utilized the preprocessing tools provided by Roboflow, specifically focusing on auto-orientation and resizing.

### **Auto-Orientation**

Digital images often come with embedded metadata, including an 'Orientation' tag, which signifies how an image should be oriented for proper viewing. However, not all software respects or interprets this metadata, leading to discrepancies in image orientation during visualization or analysis.

### Roboflow's Implementation:

Roboflow's auto-orient feature addresses this challenge by automatically adjusting images to their intended orientation, as indicated by their metadata. This process ensures consistency in orientation across the dataset, negating any inconsistencies arising from varied device orientations during image capture. It further aids in eliminating potential biases or errors that could arise during model training due to misoriented images.

### **Resizing to 640x640 Pixels**

Image size can significantly influence the computational efficiency and performance of machine learning models. Having a consistent image size is crucial for batch processing, ensuring each image aligns with the model's expected input dimensions.

### Roboflow's Implementation:

For our tasks, we opted for a standard size of 640x640 pixels. Roboflow's resize tool facilitated this by uniformly scaling images to these dimensions. While resizing, the tool maintains the original aspect ratio by padding any additional space, ensuring that the image content is not distorted. A dimension of 640x640 was chosen considering a balance between preserving image detail and computational efficiency, making it optimal for high-resolution object detection tasks, like those performed by YOLOv5.

## Augmentation

Image augmentation is a powerful tool in the deep learning toolkit, enabling models to generalize better by simulating diverse variations in training data. However, the effectiveness of external augmentation can be contingent on the specific model and its internal processing features. In this study, we explored the effects of augmentation using Roboflow on one version of our dataset (_V1_0_) and later compared results from it to a dataset with only minor manipulations (_V1_1_). YOLO has internal augmentation already included in the training process which lead to the question of the effects of manual augmentation through Roboflow.

### **Roboflow Augmentation in V1_0**

Image augmentation can involve a variety of techniques such as rotations, flips, color adjustments, and spatial translations. The intention is to increase the robustness of the model by exposing it to a diverse set of potential real-world variations during training.

### Implementation in V1_0:

In version V1_0 of our dataset, we employed a range of image augmentations provided by Roboflow. The rationale behind this was to potentially enhance the model's ability to recognize and process various transformed versions of the input data.

### **Comparative Analysis: V1_0 vs. V1_1**

Upon evaluation, it was intriguing to note that the performance metrics between V1_0 (with augmentation) and V1_1 (without augmentation) were largely analogous. There was no discernible advantage in using the augmented data from Roboflow in our specific context.

### **YOLO's Inherent Augmentation Capabilities**

The YOLO architecture incorporates its own set of data augmentation techniques during the training process.

### YOLO's Augmentation:

YOLO employs a series of random transformations on each training image, effectively augmenting the dataset on-the-fly. This includes random jittering, hue, saturation, and exposure adjustments. Moreover, YOLO has been known to perform random scaling and translations of bounding boxes, ensuring that the model remains invariant to different sizes and locations of objects within an image. This built-in augmentation mechanism aims to achieve enhanced generalization by mimicking various real-world scenarios the model might encounter post-deployment.

### **Final Version (v3_0/vfinal) Decision**

Given the inherent augmentation capabilities of YOLO, coupled with our empirical findings from the comparison between V1_0 and V1_1, we decided not to incorporate external augmentation for the final model training. Our analysis suggested that YOLO's internal augmentation mechanisms were sufficiently robust for our requirements, obviating the need for external augmentation tools like those offered by Roboflow. This decision highlights the importance of understanding model-specific behaviors and characteristics in determining preprocessing and augmentation strategies.
