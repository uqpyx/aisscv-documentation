# Group 3's Project on Artificial Intelligence in Service Systems: Application in Computer Vision

Welcome to the official Git repository of Group 3's project in Artificial Intelligence in Service Systems: Application in Computer Vision. This space is dedicated to the documentation of our methodologies, findings, and implementations. Our team has meticulously curated content that showcases the process of our project. As you explore our repository, you'll gain insights into the challenges we faced, the solutions we devised, and the future potential we envision.

> **_NOTE:_** We have created a [documentation website](https://documentation-aisscv-group3.vercel.app/) to illustrate the our documentation. to open it.
> _Disclaimer: The website does not include all of the documentation. It is only used to visualize the most important information._

## Table of Contents

- [Use Case](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/UseCase.md)
- [Formalization](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Formalization.md)
- [Model Choice](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/ModelChoice.md)
- [Architecture](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Architecture.md)
- [Data](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Data)
  - [Data Generation](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Data/DataGeneration.md)
  - [Data Set](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Data/DataSet.md)
- [Training](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Training)
  - [HPC](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Training/HPC.md)
  - [Performance](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Training/HPC.md)
- [Deployment](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Deployment.md)
- [Challenges & Solution](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Challenges%26Solutions.md)
- [Learnings](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/Learnings.md)
- [Future Outlook](https://git.scc.kit.edu/aiss_cv/documentation/-/blob/main/FutureOutlook.md)

## [All Repositories](https://git.scc.kit.edu/aiss_cv)

- [HPC Enabled Framewors](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks)
  - [HPC Script](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts)
  - [Yolov5](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov5)
  - [Yolov7](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov7)
  - [Yolov8](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov8)
- [YOLO](https://git.scc.kit.edu/aiss_cv/yolo_models)
  - [YOLOv5](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov5)
  - [YOLOv7](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov7)
  - [YOLOv8](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov8)
- [Docker Development Environemt](https://git.scc.kit.edu/aiss_cv/docker-development-environment)
- [Jetson Nano](https://git.scc.kit.edu/aiss_cv/jetson-nano)
- [REST Backend](https://git.scc.kit.edu/aiss_cv/rest-backend)
- [UI-LEGO](https://git.scc.kit.edu/aiss_cv/ui-lego)
