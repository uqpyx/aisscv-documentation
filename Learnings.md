# Learnings

1. [Labeling Quality](#labeling-quality)
2. [Nvidia Jetson Nano as Edge Device](#nvidia-jetson-nano-as-edge-device)
3. [Time Consumption of Deployment](#time-consumption-of-deployment)
4. [Temporal Correlations in Video-Extracted Frames and Dataset Partitioning](#temporal-correlations-in-video-extracted-frames-and-dataset-partitioning)

## Labeling Quality

In the domain of computer vision, especially when dealing with specific use cases such as identifying Lego Robot parts, the quality of labeled datasets is of paramount importance. An essential determinant of this quality is the cleanliness of labeling, specifically the precision of bounding box annotations. As advancements in convolutional neural networks drive forward the capabilities of computer vision, even niche applications, like ours, reap the benefits. However, irrespective of the algorithmic advancements, the foundational significance of accurately labeled data remains critical. Errors or inconsistencies in labeling can introduce noise into the dataset, leading to potential inaccuracies during model training.

For our specific application, which involves identifying different components of a Lego Robot, it's vital that the model discerns intricate details. Such granularity can often hinge on the precision of the bounding boxes used during training. Ideally, a bounding box should be 'tight', meaning it should closely encapsulate the Lego part, excluding unnecessary background while capturing the entirety of the component. Tight bounding boxes ensure that the model receives precise spatial information about each part, reducing ambiguity and enhancing differentiation capabilities, especially when components are closely situated or appear similar.

The challenge, however, lies in consistently achieving such tightness. Manual annotations, while invaluable, can introduce human biases or inconsistencies. On the other hand, automated bounding box generators might sometimes overlook subtleties or yield misaligned boxes. In our research journey with the Lego Robot, we initially believed that slight variances in bounding box tightness would have minimal effects on model performance. Yet, through rigorous testing and evaluation, we found a compelling correlation between bounding box precision and the model's accuracy in identifying Lego parts.

Given these insights, we revisited our data annotation methodologies, incorporating advanced tools and dedicated training sessions to refine the labeling processes. This ensured that our computer vision model was optimally trained to identify even the most nuanced details of the Lego Robot parts. In essence, the attention to detail in labeling, particularly the tightness of bounding boxes, is indispensable for specialized computer vision applications like ours. Such precision ensures that our model achieves the best possible performance in the complex world of Lego Robot components.

## Nvidia Jetson Nano as Edge Device

Our group's synergy and the diversity of our skills were especially evident when addressing these challenges. We delved into tools like TensorRT, recommended by some of our more experienced members, which became instrumental in model optimization.

Embarking on a journey with the NVIDIA Jetson Nano, it quickly became clear that this compact device, while powerful, would challenge our conventions about deploying computer vision models. The Nano's resource constraints, particularly in terms of memory, stood in stark contrast to the expansive capacities of traditional cloud-based servers and desktop environments.

Our earlier computer vision models, crafted with a certain degree of freedom regarding resource usage, suddenly seemed too bulky. It necessitated a shift in approach, demanding a balance between model complexity and the Nano's memory limitations.

The processing capabilities of the Jetson Nano further honed our skills. While the device boasts impressive processing power for its size, there are inherent limitations compared to larger GPU setups. Handling high-resolution imagery or executing complex operations required more than just algorithmic adjustments; it called for a holistic reconsideration of how models should be structured for real-time processing on such a device.

Adapting to these challenges wasn't just about technical adjustments. It was also a lesson in flexibility and adaptability. Leveraging tools like TensorRT emerged as an invaluable strategy in our quest for performance optimization on the Nano.

In retrospect, working with the NVIDIA Jetson Nano wasn't merely about deploying a model on an edge device. It was an enlightening experience, a testament to the ever-evolving landscape of AI and embedded computing. The challenges posed by the Nano reshaped our understanding, pushing us to innovate and adapt in ways we hadn't foreseen. Though most ML projects allow simple vertical or horizontal scaling for increased resources and therefore processing power, these solutions are traditionally only realistic when connectivity is never an issue. Edge devices are a local alternative that comes with its own set of challenges not requiring an active connection at all time.

## Time Consumption of Deployment

Deploying AI and other advanced applications on edge devices presents unique challenges compared to deploying on traditional servers or cloud infrastructure. One of the most pronounced issues revolves around the time consumption of such deployments. Edge devices, by nature, are designed to be lightweight and are often not equipped with the robust computational power or storage capacities found in more centralized systems. This translates into considerably longer deployment durations, especially when dealing with intricate AI models or large software packages.

Furthermore, our specific edge device, the Jetson Nano, is built on ARM architecture. ARM, while known for its power efficiency, does not always match up in performance to its x86 counterparts, especially when it comes to tasks like deployment or on-the-fly compilations. It's essential to factor in these inherent limitations when planning deployment schedules, as ARM-based devices can necessitate significantly more time for the same tasks.

Moreover, as technology evolves rapidly, hardware devices often reach a point where they no longer receive active support or updates from their manufacturers. This is currently the case with our Jetson Nano, which has reached the end of its support life cycle from NVIDIA. This poses additional challenges as we may not receive necessary software updates, security patches, or bug fixes. It also implies that newer software or AI models might not be compatible with our device, requiring additional time and effort in troubleshooting, adapting, or backtracking to earlier, compatible versions.

In summary, while edge devices like the Jetson Nano provide excellent opportunities for localized AI processing, it's crucial to be aware of the challenges they present, especially in terms of deployment time and ongoing support.

## **Temporal Correlations in Video-Extracted Frames and Dataset Partitioning**

The random partitioning of images into various data subsets is an established method in machine learning. However, when these images are frames derived from continuous video sequences, unique challenges arise. Such video footage is replete with temporal correlations, causing consecutive frames to frequently bear high similarities, if not being virtually identical.

If these temporally linked frames predominantly populate one subset, like a testing set, they can inadvertently introduce bias. Consequently, a deep learning model's evaluation on this subset may not offer an accurate representation of its broader generalization potential. Such a scenario could inadvertently suggest that the model is adept at handling unseen data, when it is essentially recognizing patterns from closely clustered frames.

Nevertheless, it is imperative to differentiate the goals of training and testing datasets. While the training set should encapsulate the holistic diversity of the dataset, facilitating the model in comprehending and learning intricate patterns, the testing set should act as a genuine \***\*benchmark \*\***test. It should comprise images that are entirely unfamiliar to the model, mirroring real-world unforeseen data scenarios. If there exists an overlap or even a subtle correlation between the training and testing sets, the very essence of evaluating a model's generalization ability gets compromised.

Our experience underscores the importance of this delineation. A critical takeaway from our project is the necessity of deliberately crafting a test set – one that remains uncorrelated to the training data. In retrospect, allocating resources to generate exclusive images for testing, irrespective of the extensive diversity of the training set, would have rendered a more precise assessment of our model's real-world applicability.

In summation, we must carefully navigate the intricacies of dataset partitioning, especially with video-derived frames. The lessons from our endeavors have fortified this understanding, emphasizing the significance of distinct test datasets in future undertakings.
