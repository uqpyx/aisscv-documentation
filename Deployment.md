# Deployment

- [Complications with Nvidia Stock Image](#complications-with-nvidia-stock-image)
- [Jetson Nano Ubuntu Image](#jetson-nano-ubuntu-image)
- [Trying TensorRT](#trying-tensorrt)
- [Inference Script](#inference-script)

## Complications with Nvidia Stock Image

Deploying our YOLO model to the NVIDIA Jetson Nano presented a unique set of challenges, primarily stemming from compatibility issues with the stock image provided by NVIDIA.

One of the primary issues was the incompatibility with Python 3.6, which was bundled with the stock image. Python's ecosystem is vast, and certain libraries and dependencies are strictly tied to specific versions. In our scenario, getting all the dependencies to work harmoniously proved difficult.

The core complications arose when trying to install and ensure the proper functioning of essential libraries and frameworks, including PyTorch with CUDA support, torchvision, TensorRT, and Ultralytics. Each of these components has its own set of dependencies, and they must be perfectly synchronized to ensure seamless operation. The complexity increases when considering CUDA, which requires a precise match between the GPU architecture, PyTorch, and the CUDA toolkit.

Furthermore, Ultralytics, which is pivotal for YOLO implementations, added another layer of intricacy. Getting it to work in harmony with the other components was no small feat.

Despite the robustness and capabilities that the Jetson Nano offers, especially for edge computing and real-time AI tasks, the stock image's constraints made the deployment phase unexpectedly intricate. This is probably caused by its ending support lifecycle.

In reflection, it emphasizes the importance of having streamlined software compatibility, especially in deployment environments tailored for specific hardware. While NVIDIA provides a powerful platform with the Jetson Nano, ensuring a smooth deployment requires meticulous planning and a deep understanding of both software and hardware interdependencies.

After two weeks of continuous failure, we were made aware of the Jetson Nano Ubuntu Image by Qengineering.

## Jetson Nano Ubuntu Image

The Jetson Nano with Ubuntu 20.04 OS image has been specifically tailored for the Jetson Nano platform. It operates on the foundation of Ubuntu 20.04 with Python 3.8 and comes equipped with a suite of preinstalled software. Notable among these are OpenCV 4.8.0, TensorFlow 2.4.1, PyTorch 1.13.0, TorchVision 0.14.0, TensorRT 8.0.1.6, and Jtop 4.2.1. It's important to note that while this image supports TensorRT 8.0.1.6, newer versions of TensorRT require CUDA 11 or later. This is a version not supported on the Jetson Nano. Similarly, TensorFlow versions 2.5 and above, as well as PyTorch 2.0, demand CUDA 11, which, due to incompatibility issues, cannot be installed on the Jetson Nano.

For the installation process, users are advised to have a minimum of a 32 GB SD card. However, given the expansive nature of the software, a 64 GB or larger SD card is recommended to ensure sufficient operational space. We are using a 128 GB SD card. Tools like Imager, Rufus or balenaEtcher can be employed to flash the image onto the SD card.

After flashing the image to the SD card, we only needed to install the ultralytics package via:

```jsx
pip3 install ultralytics
```

## Trying TensorRT

In our endeavor to optimize our model's performance, we ventured into leveraging TensorRT. The process commenced with the conversion of our model's weights into an ONNX format, which was then subsequently transformed into an engine file tailored for TensorRT. However, our efforts met with a roadblock during the final phase — the inference step. Unfortunately, despite our efforts, this crucial step failed. Employing TensorRT for inference presents significant benefits. TensorRT is designed to optimize and accelerate the inference of deep learning models, and integrating it can lead to enhanced speed and reduced latency, especially on NVIDIA hardware. Furthermore, when compared to using weights directly in YOLOv8 Ultralytics for inference, TensorRT could have offered superior performance in terms of runtime efficiency while maintaining a comparable level of accuracy. Such benefits would have been very valuable for our use case.

## Inference Script

We are running the inference via this script:

```jsx
from ultralytics import YOLO

# Load the YOLO model from the given file
model = YOLO("best.pt")

# Get prediction results. This continuously streams for source "1" with a confidence threshold of 0.5
results = model.predict(source="1", conf=0.5, show=True, stream=True, device="0")
```
